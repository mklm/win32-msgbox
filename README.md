# win32-msgbox

**win32-msgbox** is a ([Chicken](http://www.call-cc.org)) Scheme interface to
the Win32
[MessageBox](http://msdn.microsoft.com/en-us/library/windows/desktop/ms645505(v=vs.85).aspx)
function.


```
[procedure] (msgbox MSG #!key (title "")
                              (buttons 'ok)
                              (default-button 1)
                              (icon 'none)
                              (foreground #t)
                              (topmost #f))
```

Valid symbols for the *buttons* option are:

* ok
* ok/cancel
* abort/retry/ignore
* yes/no/cancel
* yes/no
* retry/cancel
* cancel/try/continue

Valid symbols for the *icon* option are:

* none
* stop
* question
* exclamation
* information

**msgbox** returns the symbol associated to each button (e.g. the *retry*
button returns the *'retry* symbol).

## Requirements

A Windows platform of course.

## Usage

```
#!scheme
(use win32-msgbox)
```

The *-Wl,--subsystem,windows* linker switch ensures that the application is
built as a Windows GUI application, and not as a console application.

```
csc -Wl,--subsystem,windows test.scm
```

### Example 1:

```
#!scheme
(use win32-msgbox)

(msgbox "Your public IP address is:\n\n7.19.211.65"
        title: "Info"
        buttons: 'ok
        icon: 'information)
```
![msgbox1_img](https://bitbucket.org/mklm/win32-msgbox/wiki/msgbox1.png)

### Example 2:

```
#!scheme
(use win32-msgbox)

(define user-input (msgbox "Resource not available\nDo you want to try again?"
                            title: "Warning"
                            buttons: 'retry/cancel
                            icon: 'exclamation))

(or (eq? user-input 'retry)
    (exit 1))
...
```
![msgbox2_img](https://bitbucket.org/mklm/win32-msgbox/wiki/msgbox2.png)

## About this software

The source code is hosted at
[Bitbucket](https://bitbucket.org/mklm/win32-msgbox/). Feel free to send pull
requests or open an issue there. Alternatively, send an e-mail to the
[chicken-users](mailto:chicken-users@nongnu.org) mailing list for information
or requests.

### Author

[Michele La Monaca](mailto:win32_msgbox@lamonaca.net)

### Version history

**1.1** Fixed build problem in mingw/msys

**1.0** Initial release

### License

```
#!text
Copyright (c) 2013-2014
Michele La Monaca (win32-msgbox@lamonaca.net)
All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:

 1) Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 2) Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.

 3) Neither the name of the author nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
```
